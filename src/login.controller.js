define([], function () {
    'use strict';

    function LoginController($state, $iqApiAuth, $iqAdminAuth) {
        var vm = this;

        vm.data = {};
        vm.error = '';
        vm.isLoading = false;

        vm.login = login;

        function login() {
            vm.isLoading = true;
            $iqApiAuth.login(vm.data)
                .then(function () {
                    $iqAdminAuth.redirectTo()
                        .then(function (state) {
                            $state.go(state);
                        });
                })
                .catch(function (response) {
                    vm.error = response.data.message;
                })
                .finally(function () {
                    vm.isLoading = false;
                });
        }
    }

    LoginController.$inject = ['$state', '$iq.api.auth', '$iq.admin.auth'];

    return LoginController;
});
