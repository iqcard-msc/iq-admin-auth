define([], function () {
    'use strict';


    function AdminAuthProvider() {
        var provider = this;

        provider.$get = AdminAuthService;

        provider.redirectTo = 'main';

        AdminAuthService.$inject = ['$q', '$injector'];
        function AdminAuthService($q, $injector) {
            var service = {};
            var redirectPromise;

            function isFunction(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            }

            if (typeof provider.redirectTo === 'string') {
                redirectPromise = function () {
                    return $q.when(provider.redirectTo);
                }
            } else if (isFunction(provider.redirectTo)) {
                // console.log('func');
                redirectPromise = function () {
                    return $q.when($injector.invoke(provider.redirectTo));
                }
            }
            // console.log(redirectPromise);
            service.redirectTo = redirectPromise;

            return service;
        }
    }

    return AdminAuthProvider;
});
