define([], function () {
    'use strict';

    function LogoutController($rootScope, $state, $iqApiAuth) {
        $iqApiAuth.logout()
            .then(function () {
                $rootScope.user = null;
                $state.go('auth.login');
            });
    }

    LogoutController.$inject = ['$rootScope', '$state', '$iq.api.auth'];

    return LogoutController;
});
