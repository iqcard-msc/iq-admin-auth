define([], function () {
    'use strict';

    function routes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('auth', {
                abstract: true,
                template: '<ui-view>',
            })
            .state('auth.login', {
                url: '/login',
                templateUrl: 'admin/auth/login.form.html',
                controller: 'LoginController',
                controllerAs: 'loginVm'
            })
            .state('auth.logout', {
                url: '/logout',
                controller: 'LogoutController'
            });
    }

    routes.$inject = ['$stateProvider', '$urlRouterProvider'];

    return routes;
});
