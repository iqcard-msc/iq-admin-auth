define([], function () {
    'use strict';

    run.$inject = ['$q', '$state', '$rootScope', '$iq.api.auth', '$iq.admin.auth'];
    function run($q, $state, $rootScope, $iqApiAuth, $iqAdminAuth) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            if (toState.name === 'auth.login') {
                return;
            }

            if (toState.data && toState.data.permissions && toState.data.permissions.length) {
                event.preventDefault();
                return $iqApiAuth.hasPermissions(toState.data.permissions)
                    .then(permissionsResolver);
            }

            ////////////////////

            function permissionsResolver(hasAccess) {
                if (hasAccess) {
                    return $state.go(toState.name, toParams, {notify: false})
                        .then(function () {
                            $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
                        });
                }

                alert('Недостаточно прав');
                $rootScope.$broadcast('$stateChangeError', toState, toParams, fromState, fromParams, 'Permission denied');

                if (!fromState.name) {
                    return $iqAdminAuth.redirectTo().then(function (state) {
                        return $state.go(state);
                    });
                }
            }

        });
    }

    return run;
});
