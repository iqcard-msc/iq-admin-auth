define('auth.module',[
    'angular',

    './auth.routes',
    './auth.run',
    './auth.templates',
    './auth.provider',
    './login.controller',
    './logout.controller',

    'angular-ui-router',
    'iq-api'
], function (
    angular,
    // config,
    routes,
    run,
    templates,
    AdminAuthProvider,
    LoginController,
    LogoutController
) {
    'use strict';

    var module = angular.module('iq.admin.auth', [
        'ui.router',
        'iq.api'
    ]);

    // module.config(config);
    module.config(routes);

    module.run(run);
    module.run(templates);

    module.provider('$iq.admin.auth', AdminAuthProvider);
    module.controller('LoginController', LoginController);
    module.controller('LogoutController', LogoutController);

    return module;
});
