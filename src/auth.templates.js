define([], function () {
    function templateCache($templateCache) {
          'use strict';

  $templateCache.put('admin/auth/login.form.html',
    "<div class=\"iq-centered\">\n" +
    "    <div class=\"iq-centered__wrap iq-block iq-block_style_login\">\n" +
    "        <header class=\"iq-block__header\">\n" +
    "            <h1 class=\"iq-block__header__heading\">Вход в личный кабинет администратора</h1>\n" +
    "        </header>\n" +
    "        <div class=\"iq-block__container\">\n" +
    "            <form name=\"form\" ng-submit=\"loginVm.login()\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"username\" class=\"control-label\">Имя пользователя</label>\n" +
    "                    <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" ng-model=\"loginVm.data.username\" required>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"password\" class=\"control-label\">Пароль</label>\n" +
    "                    <input type=\"password\" id=\"password\" class=\"form-control\" name=\"password\" ng-model=\"loginVm.data.password\" required>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-primary btn-lg btn-block\"\n" +
    "                        ng-disabled=\"loginVm.isLoading || form.$invalid\"\n" +
    "                        ng-class=\"{ 'btn-loading': loginVm.isLoading }\">Войти</button>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" ng-if=\"loginVm.error\">\n" +
    "                        <div class=\"alert alert-danger\" ng-bind=\"loginVm.error\"></div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );

    }
    templateCache.$inject = ['$templateCache'];
    return templateCache;
});