define('auth.routes',[], function () {
    'use strict';

    function routes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('auth', {
                abstract: true,
                template: '<ui-view>',
            })
            .state('auth.login', {
                url: '/login',
                templateUrl: 'admin/auth/login.form.html',
                controller: 'LoginController',
                controllerAs: 'loginVm'
            })
            .state('auth.logout', {
                url: '/logout',
                controller: 'LogoutController'
            });
    }

    routes.$inject = ['$stateProvider', '$urlRouterProvider'];

    return routes;
});

define('auth.run',[], function () {
    'use strict';

    run.$inject = ['$q', '$state', '$rootScope', '$iq.api.auth', '$iq.admin.auth'];
    function run($q, $state, $rootScope, $iqApiAuth, $iqAdminAuth) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            if (toState.name === 'auth.login') {
                return;
            }

            if (toState.data && toState.data.permissions && toState.data.permissions.length) {
                event.preventDefault();
                return $iqApiAuth.hasPermissions(toState.data.permissions)
                    .then(permissionsResolver);
            }

            ////////////////////

            function permissionsResolver(hasAccess) {
                if (hasAccess) {
                    return $state.go(toState.name, toParams, {notify: false})
                        .then(function () {
                            $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
                        });
                }

                alert('Недостаточно прав');
                $rootScope.$broadcast('$stateChangeError', toState, toParams, fromState, fromParams, 'Permission denied');

                if (!fromState.name) {
                    return $iqAdminAuth.redirectTo().then(function (state) {
                        return $state.go(state);
                    });
                }
            }

        });
    }

    return run;
});

define('auth.templates',[], function () {
    function templateCache($templateCache) {
          'use strict';

  $templateCache.put('admin/auth/login.form.html',
    "<div class=\"iq-centered\">\n" +
    "    <div class=\"iq-centered__wrap iq-block iq-block_style_login\">\n" +
    "        <header class=\"iq-block__header\">\n" +
    "            <h1 class=\"iq-block__header__heading\">Вход в личный кабинет администратора</h1>\n" +
    "        </header>\n" +
    "        <div class=\"iq-block__container\">\n" +
    "            <form name=\"form\" ng-submit=\"loginVm.login()\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"username\" class=\"control-label\">Имя пользователя</label>\n" +
    "                    <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" ng-model=\"loginVm.data.username\" required>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"password\" class=\"control-label\">Пароль</label>\n" +
    "                    <input type=\"password\" id=\"password\" class=\"form-control\" name=\"password\" ng-model=\"loginVm.data.password\" required>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-primary btn-lg btn-block\"\n" +
    "                        ng-disabled=\"loginVm.isLoading || form.$invalid\"\n" +
    "                        ng-class=\"{ 'btn-loading': loginVm.isLoading }\">Войти</button>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" ng-if=\"loginVm.error\">\n" +
    "                        <div class=\"alert alert-danger\" ng-bind=\"loginVm.error\"></div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );

    }
    templateCache.$inject = ['$templateCache'];
    return templateCache;
});
define('auth.provider',[], function () {
    'use strict';


    function AdminAuthProvider() {
        var provider = this;

        provider.$get = AdminAuthService;

        provider.redirectTo = 'main';

        AdminAuthService.$inject = ['$q', '$injector'];
        function AdminAuthService($q, $injector) {
            var service = {};
            var redirectPromise;

            function isFunction(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            }

            if (typeof provider.redirectTo === 'string') {
                redirectPromise = function () {
                    return $q.when(provider.redirectTo);
                }
            } else if (isFunction(provider.redirectTo)) {
                // console.log('func');
                redirectPromise = function () {
                    return $q.when($injector.invoke(provider.redirectTo));
                }
            }
            // console.log(redirectPromise);
            service.redirectTo = redirectPromise;

            return service;
        }
    }

    return AdminAuthProvider;
});

define('login.controller',[], function () {
    'use strict';

    function LoginController($state, $iqApiAuth, $iqAdminAuth) {
        var vm = this;

        vm.data = {};
        vm.error = '';
        vm.isLoading = false;

        vm.login = login;

        function login() {
            vm.isLoading = true;
            $iqApiAuth.login(vm.data)
                .then(function () {
                    $iqAdminAuth.redirectTo()
                        .then(function (state) {
                            $state.go(state);
                        });
                })
                .catch(function (response) {
                    vm.error = response.data.message;
                })
                .finally(function () {
                    vm.isLoading = false;
                });
        }
    }

    LoginController.$inject = ['$state', '$iq.api.auth', '$iq.admin.auth'];

    return LoginController;
});

define('logout.controller',[], function () {
    'use strict';

    function LogoutController($rootScope, $state, $iqApiAuth) {
        $iqApiAuth.logout()
            .then(function () {
                $rootScope.user = null;
                $state.go('auth.login');
            });
    }

    LogoutController.$inject = ['$rootScope', '$state', '$iq.api.auth'];

    return LogoutController;
});

define('auth.module',[
    'angular',

    './auth.routes',
    './auth.run',
    './auth.templates',
    './auth.provider',
    './login.controller',
    './logout.controller',

    'angular-ui-router',
    'iq-api'
], function (
    angular,
    // config,
    routes,
    run,
    templates,
    AdminAuthProvider,
    LoginController,
    LogoutController
) {
    'use strict';

    var module = angular.module('iq.admin.auth', [
        'ui.router',
        'iq.api'
    ]);

    // module.config(config);
    module.config(routes);

    module.run(run);
    module.run(templates);

    module.provider('$iq.admin.auth', AdminAuthProvider);
    module.controller('LoginController', LoginController);
    module.controller('LogoutController', LogoutController);

    return module;
});

define('iq-admin-auth',['./auth.module'], function () {
    'use strict';


});

