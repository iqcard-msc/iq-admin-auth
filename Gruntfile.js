module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        ngtemplates: {
            app: {
                cwd: 'src',
                src: '*.html',
                dest: 'src/auth.templates.js',
                options: {
                    prefix: 'admin/auth/',
                    module: 'iq.admin.auth',
                    bootstrap:  function(module, script) {
                        return "" +
                        "define([], function () {\n" +
                        "    function templateCache($templateCache) {\n" +
                        "        " + script + "\n" +
                        "    }\n" +
                        "    templateCache.$inject = ['$templateCache'];\n" +
                        "    return templateCache;\n" +
                        "});";
                    }
                }
            }
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: './src/',
                    name: 'iq-admin-auth',
                    out: 'iq-admin-auth.js',
                    optimize: 'none',
                    paths: {
                        'angular': 'empty:',
                        'angular-ui-router': 'empty:',
                        'iq-api': 'empty:'
                    }
                }
            }
        },
        watch: {
            default: {
                files: ['src/**/*.js'],
                tasks: ['requirejs']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('start', function () {
        require('./server.js');
        grunt.task.run('watch');
    });

    grunt.registerTask('default', ['ngtemplates', 'requirejs']);
};
