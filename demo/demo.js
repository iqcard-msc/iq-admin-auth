require.config({
    paths: {
        'jquery': 'https://code.jquery.com/jquery-2.1.4.min',
        'domReady': 'https://cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady',
        'angular': '../bower_components/angular/angular',
        'angular-ui-router': '../bower_components/angular-ui-router/release/angular-ui-router',
        'iq-api': '../bower_components/iq-api/iq-api',
        'iq-admin-auth': '../iq-admin-auth'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'angular': {
            exports: 'angular'
        },
        'angular-ui-router': {
            deps: ['angular']
        }
    }
});

require(['domReady!', 'angular', 'iq-admin-auth'], function (document, angular) {
    'use strict';

    var app = angular.module('demo', [
        'iq.admin.auth'
    ]);

    app.config(['$stateProvider', '$iq.apiProvider', '$iq.admin.authProvider',
        function ($stateProvider, $iqApiProvider, $adminAuthProvider) {
        $iqApiProvider.urlPrefix = '@@api';

        // redirect.$inject = ['$q'];
        function redirect() {
            return 'demo.foo';
            // return $q.when('demo');
        }

        currentUser.$inject = ['$iq.api.auth'];
        function currentUser($iqApiAuth) {
            return $iqApiAuth;
        }

        $adminAuthProvider.redirectTo = redirect;

        $stateProvider
            .state('demo', {
                url: '',
                templateUrl: 'demo/demo.html'
            })
            .state('demo.foo', {
                url: '/foo',
                template: 'foo',
                data: {
                    permissions: ['ADMIN']
                }
            })
            .state('demo.bar', {
                url: '/bar',
                template: 'bar',
                data: {
                    permissions: ['PERATOR']
                },
                controller: function () {
                    console.log('BAR!!');
                }
            })
            .state('demo.baz', {
                url: '/baz',
                template: 'baz',
                data: {

                }
            });
    }]);

    angular.bootstrap(document, ['demo']);
});
