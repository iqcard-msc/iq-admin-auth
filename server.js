var http = require('http'),
    url  = require('url'),
    request = require('request'),
    finalhandler = require('finalhandler'),
    serveStatic  = require('serve-static');

var PORT   = 9000,
    REMOTE = 'http://qa1.iqcard.ru:8090';

var serve = serveStatic("./");

var DELAY = 0; // in ms

http.createServer(function(req, res, next) {
    'use strict';

    function onResponse(err, _res, body) {
        if (err) {
            res.writeHead(500);
            res.end();
        } else {
            res.writeHead(_res.statusCode, _res.headers);
            res.end(body);
        }
    }

    var path = req.url.split('/');

    if (path[1] === '@@api') {

        path.splice(0, 2, REMOTE);

        var _url = path.join('/');

        console.log('request to "' + _url + '"');

        req.pipe(request({
            url: _url,
            method: req.method,
            headers: req.headers
        })).pipe(res);

        //
        // var body = "";
        //
        // req.on('data', function (chunk) {
        //     body += chunk;
        // });
        //
        // req.on('end', function () {
        //     setTimeout(function () {
        //         request({
        //             url:     _url,
        //             method:  req.method,
        //             headers: req.headers,
        //             body:    body
        //         }, onResponse);
        //     }, DELAY);
        //
        // });
    } else {
        if (req.url.indexOf('html') > -1
            || req.url.indexOf('js') > -1
            || req.url.indexOf('img') > -1
            || req.url.indexOf('css') > -1
            || req.url.indexOf('font') > -1
            ) {
            //next();
        } else {
            req.url = 'demo/index.html';
            //var done = finalhandler(req, res);
            //serve(req, res, done);
        }
        var done = finalhandler(req, res);
        serve(req, res, done);
    }
}).listen(PORT);

console.log("running on", PORT);
